import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:poppu/src/pages/home.dart';
import 'package:poppu/src/ui/walkthrough/walkthrough-1.dart';

final FirebaseAuth _fireAuth = FirebaseAuth.instance;

class PoppuApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _PoppuAppState();
  }
}

class _PoppuAppState extends State<PoppuApp> {
  FirebaseUser user;
  final List<Widget> _children = [
    HomePage(),
    FirstWalkthroughPage(),
  ];
  int _currentIndex = 1;

  getCurrentUser() async {
    user = await _fireAuth.currentUser();
    if (user != null) {
      setState(() {
        _currentIndex = 0;
      });
    } else {
      setState(() {
        _currentIndex = 1;
      });
    }
  }

  @override
  void initState() {
    getCurrentUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          primaryColor: Colors.white, accentColor: Colors.yellowAccent),
      home: _children[_currentIndex],
    );
  }
}
