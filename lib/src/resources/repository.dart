import 'package:poppu/src/resources/user_provider.dart';

// Central class where data will flow from BLOC
class Repository {
  final userProvider = new UserProvider();

}