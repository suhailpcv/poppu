import 'package:poppu/src/models/user.dart';
import 'package:scoped_model/scoped_model.dart';

class UserModel extends Model {
  User user;
  User get _user => user;
}