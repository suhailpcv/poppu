import 'package:flutter/material.dart';

class User {
  String name;
  String phoneNumber;
  String bio;
  String imageURL;

  User({
    @required this.name,
    @required this.phoneNumber,
    this.bio,
    this.imageURL
  });
}