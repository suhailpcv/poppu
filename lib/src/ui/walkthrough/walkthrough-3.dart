import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:animated_background/animated_background.dart';
import 'package:flutter/material.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:poppu/src/ui/walkthrough/walkthrough-4.dart';
import 'package:poppu/src/ui/widgets/reactive_refresh_indicator.dart';

enum AuthStatus { PHONE_AUTH, SMS_AUTH, EMPTY_AUTH }

class ThirdWalkthroughPage extends StatefulWidget {
  final String name;
  ThirdWalkthroughPage(this.name);

  @override
  State<StatefulWidget> createState() {
    return _ThirdWalkthroughState();
  }
}

class _ThirdWalkthroughState extends State<ThirdWalkthroughPage>
    with TickerProviderStateMixin {
  static const String TAG = "WALKTHROUGH-3";
  AuthStatus status = AuthStatus.PHONE_AUTH;

  // Controllers
  TextEditingController smsCodeController = TextEditingController();
  // Variables
  String _countryCode = '';
  String _phoneNumber = '';
  String _verificationId;
  String _message;
  Timer _codeTimer;

  bool _isRefreshing = false;
  bool _codeTimedOut = false;
  bool _codeVerified = false;
  int oneMinute = 60;
  Duration _timeOutDuration = const Duration(seconds: 1);

  // Firebase
  final FirebaseAuth _fireAuth = FirebaseAuth.instance;

  @override
  void dispose() {
    _codeTimer?.cancel();
    super.dispose();
  }

  String _phoneInputValidator() {
    if (_phoneNumber.isEmpty) {
      return "Your phone number can't be empty";
    } else if (_phoneNumber.length < 10) {
      return "This phone number is incomplete";
    }
    return null;
  }

  String _smsInputValidator() {
    if (smsCodeController.text.isEmpty) {
      return "Your verification code can't be empty";
    } else if (smsCodeController.text.length < 6) {
      return "This verification code is incomplete";
    }
    return null;
  }

  _verificationCompleted(FirebaseUser user) async {
    print('$TAG - onVerificationCompleted, user : $user');
    if (await _onCodeVerified(user)) {
      await _finishSignIn(user);
    } else {
      setState(() {
        status = AuthStatus.SMS_AUTH;
      });
    }
  }

  _verificationFailed(AuthException authException) {
    print(
        'Code verification failed : ${authException.code} - ${authException.message}');
    _showErrorSnackbar("We couldn't verify your code. Please try again");
  }

  // PhoneCodeSent
  _codeSent(String verificationId, [int forceResendingToken]) async {
    print('$TAG - Verification code sent to number $_phoneNumber');
    _codeTimer = Timer.periodic(_timeOutDuration, (Timer timer) {
      setState(() {
        if (oneMinute < 1) {
          _codeTimedOut = true;
          timer.cancel();
          oneMinute = 60;
        } else {
          oneMinute -= 1;
        }
      });
    });
    _updateRefreshing(false);
    setState(() {
      _verificationId = verificationId;
      status = AuthStatus.SMS_AUTH;
    });
  }

  // PhoneCodeAutoRetrievalTimeout
  _codeAutoRetrievalTimeout(String verificationId) {
    print('$TAG - onCodeTimeOut');
    _updateRefreshing(false);
    setState(() {
      _verificationId = verificationId;
      _codeTimedOut = true;
    });
  }

  _finishSignIn(FirebaseUser user) async {
    await _onCodeVerified(user).then((result) {
      if (result) {
        print('Finished Signin');
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => FourthWalkthroughPage()),
        );
      } else {
        setState(() {
          this.status = AuthStatus.SMS_AUTH;
        });
        _showErrorSnackbar(
            'We couldn\'t create your profile for now, please try again later');
      }
    });
  }

  _showErrorSnackbar(String message) {
    _updateRefreshing(false);
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(message),
    ));
  }

  // Async Functions

  Future<void> _signInWithPhoneNumber() async {
    final AuthCredential credential = PhoneAuthProvider.getCredential(
        verificationId: _verificationId, smsCode: smsCodeController.text);
    final FirebaseUser user = await _fireAuth.signInWithCredential(credential);
    final FirebaseUser currentUser = await _fireAuth.currentUser();
    await _fireAuth.currentUser().then((val) {
      UserUpdateInfo updateUser = UserUpdateInfo();
      updateUser.displayName = widget.name;
      val.updateProfile(updateUser);
    });
    assert(user.uid == currentUser.uid);
    setState(() {
      if (user != null) {
        _message = 'Successfully signed in, uid: ' + user.uid;
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => FourthWalkthroughPage()),
        );
      } else {
        _message = 'Sign in failed';
      }
    });
  }

  Future<Null> _updateRefreshing(bool isRefreshing) async {
    if (_isRefreshing) {
      setState(() {
        this._isRefreshing = false;
      });
    }
    setState(() {
      this._isRefreshing = isRefreshing;
    });
  }

  Future<bool> _onCodeVerified(FirebaseUser user) async {
    final isUserValid = (user != null &&
        (user.phoneNumber != null && user.phoneNumber.isNotEmpty));

    if (isUserValid) {
      setState(() {
        status = AuthStatus.EMPTY_AUTH;
      });
    } else {
      _showErrorSnackbar('We couldn\'t verify your code. Please try again');
    }

    return isUserValid;
  }

  Future<Null> _onRefresh() async {
    switch (status) {
      case AuthStatus.EMPTY_AUTH:
        break;
      case AuthStatus.PHONE_AUTH:
        return await _submitPhoneNumber();
        break;
      case AuthStatus.SMS_AUTH:
        return await _submitSmsCode();
        break;
    }
  }

  Future<Null> _submitPhoneNumber() async {
    final error = _phoneInputValidator();
    if (error != null) {
      print('Submit Phone Number : $error');
      _updateRefreshing(false);
      setState(() {
        _message = error;
      });
      return null;
    } else {
      _updateRefreshing(false);
      setState(() {
        _message = null;
      });
      final result = await _verifyPhoneNumber();
      return result;
    }
  }

  Future<Null> _submitSmsCode() async {
    final error = _smsInputValidator();
    if (error != null) {
      _updateRefreshing(false);
      _showErrorSnackbar(error);
      return null;
    } else {
      if (_codeVerified) {
        await _finishSignIn(await _fireAuth.currentUser());
      } else {
        await _signInWithPhoneNumber();
      }
      return null;
    }
  }

  Future<Null> _verifyPhoneNumber() async {
    await _fireAuth.verifyPhoneNumber(
        phoneNumber: _phoneNumber,
        timeout: _timeOutDuration,
        codeSent: _codeSent,
        codeAutoRetrievalTimeout: _codeAutoRetrievalTimeout,
        verificationCompleted: _verificationCompleted,
        verificationFailed: _verificationFailed);
    return null;
  }

  Widget middleSection() {
    return Expanded(
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Verify your phone number for\n unique identification',
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.w400, fontSize: 18.0),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.fromLTRB(0.0, 25.0, 0.0, 0.0),
                  width: 100.0,
                  child: CountryCodePicker(
                    onChanged: (value) {
                      _countryCode = value.toString();
                    },
                    initialSelection: 'MY',
                    favorite: ['+60', 'MY'],
                  ),
                ),
                middleNumberContainer(),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Container middleNumberContainer() {
    return Container(
      margin: EdgeInsets.fromLTRB(0.0, 25.0, 0.0, 0.0),
      width: 250.0,
      child: TextField(
        textAlign: TextAlign.center,
        keyboardType: TextInputType.number,
        onChanged: (value) {
          _phoneNumber = _countryCode + value;
        },
        textInputAction: TextInputAction.continueAction,
        textCapitalization: TextCapitalization.words,
        style: TextStyle(color: Colors.deepPurple, fontWeight: FontWeight.bold),
        maxLines: 1,
        decoration: InputDecoration(
          fillColor: Colors.white,
          filled: true,
          hintText: 'Enter Phone Number',
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30.0),
            borderSide: BorderSide(color: Colors.black, width: 2.0),
          ),
        ),
      ),
    );
  }

  Widget bottomSection(BuildContext context) {
    return Flexible(
      child: Container(
        padding: EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            FlatButton(
              child: Text('Back'),
              onPressed: () => {Navigator.of(context).pop()},
              padding: const EdgeInsets.all(10.0),
            ),
            OutlineButton(
              child: Text('Submit'),
              splashColor: Colors.lightBlueAccent,
              borderSide: BorderSide(width: 2, color: Colors.lightBlue),
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0)),
              onPressed: (this.status == AuthStatus.EMPTY_AUTH)
                  ? null
                  : () => _updateRefreshing(true),
              padding: const EdgeInsets.all(10.0),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    debugPrint(widget.name);
    return Scaffold(
      body: AnimatedBackground(
        behaviour: BubblesBehaviour(),
        vsync: this,
        child: Container(
          child: ReactiveRefreshIndicator(
            onRefresh: _onRefresh,
            isRefreshing: _isRefreshing,
            child: Container(
              child: _buildBody(),
            ),
          ),
        ),
      ),
    );
  }

  Widget _phoneAuthBody() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        middleSection(),
        bottomSection(context),
      ],
    );
  }

  Widget _buildResendSmsWidget() {
    return InkWell(
      onTap: () async {
        if (_codeTimedOut) {
          await _verifyPhoneNumber();
        } else {
          _showErrorSnackbar("You can't retry yet!");
        }
      },
      child: Padding(
        padding: const EdgeInsets.all(4.0),
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            style: TextStyle(color: Colors.black54),
            text: "If your code does not arrive in 1 minute, touch",
            children: <TextSpan>[
              TextSpan(
                text: " here",
                style: TextStyle(
                  color: Colors.blueGrey,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildConfirmInputButton() {
    final theme = Theme.of(context);
    return IconButton(
      icon: Icon(Icons.check),
      color: Colors.deepPurple,
      disabledColor: theme.buttonColor,
      onPressed: (this.status == AuthStatus.EMPTY_AUTH)
          ? null
          : () => _updateRefreshing(true),
    );
  }

  Widget _buildSmsCodeInput() {
    final enabled = this.status == AuthStatus.SMS_AUTH;
    return TextField(
      keyboardType: TextInputType.number,
      enabled: enabled,
      textAlign: TextAlign.center,
      controller: smsCodeController,
      maxLength: 6,
      onSubmitted: (text) => _updateRefreshing(true),
      style: TextStyle(fontSize: 32, color: Colors.black),
      decoration: InputDecoration(
          counterText: "",
          enabled: enabled,
          hintText: "- - - - - -",
          hintStyle: TextStyle(color: Colors.grey)),
    );
  }

  Widget _buildTimerText() {
    return Text('00:$oneMinute');
  }

  Widget _smsAuthBody() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 24.0),
          child: Text(
            "Verification code",
            textAlign: TextAlign.center,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 2.0, horizontal: 64.0),
          child: Flex(
            direction: Axis.horizontal,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Flexible(flex: 1, child: _buildTimerText()),
              Flexible(flex: 3, child: _buildSmsCodeInput()),
              Flexible(flex: 1, child: _buildConfirmInputButton())
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24.0),
          child: _buildResendSmsWidget(),
        )
      ],
    );
  }

  Widget _buildBody() {
    Widget body;
    switch (status) {
      case AuthStatus.PHONE_AUTH:
        body = _phoneAuthBody();
        break;
      case AuthStatus.SMS_AUTH:
      case AuthStatus.EMPTY_AUTH:
        body = _smsAuthBody();
        break;
    }
    return body;
  }
}
