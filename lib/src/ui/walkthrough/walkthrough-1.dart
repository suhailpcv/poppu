import 'package:flutter/material.dart';
import 'package:animated_background/animated_background.dart';
import 'package:poppu/src/ui/walkthrough/walkthrough-2.dart';

class FirstWalkthroughPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _FirstWalkthroughState();
  }
}

class _FirstWalkthroughState extends State<FirstWalkthroughPage>
    with TickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
  }

  Positioned positionedButton() {
    return Positioned(
      top: 600,
      left: 160,
      child: OutlineButton(
        child: Text('Next'),
        splashColor: Colors.lightBlueAccent,
        borderSide: BorderSide(width: 2, color: Colors.lightBlue),
        shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0)),
        onPressed: () => {
              Navigator.push(
                context,
                new MaterialPageRoute(
                  builder: (BuildContext context) =>
                      new SecondWalkthroughPage(),
                ),
              )
            },
        padding: const EdgeInsets.all(10.0),
      ),
    );
  }

  Widget pageStack() {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Center(
            child: Text(
          'Hello.',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 50.0),
        )),
        positionedButton()
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnimatedBackground(
        behaviour: BubblesBehaviour(),
        vsync: this,
        child: pageStack(),
      ),
    );
  }
}
