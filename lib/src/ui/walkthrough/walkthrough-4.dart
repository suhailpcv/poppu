import 'package:animated_background/animated_background.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:poppu/src/pages/home.dart';

class FourthWalkthroughPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _FourthWalkthroughState();
  }
}

class _FourthWalkthroughState extends State<FourthWalkthroughPage>
    with TickerProviderStateMixin {
  bool _isButtonDisabled = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnimatedBackground(
        behaviour: BubblesBehaviour(),
        vsync: this,
        child: _buildBody(),
      ),
    );
  }

  Widget _buildBody() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        middleSection(),
        bottomSection(context),
      ],
    );
  }

  Widget middleSection() {
    return Expanded(
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
              child: Text(
                'Give Poppu access to your\n location to enjoy all features',
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
              ),
            ),
            borderContainer(),
          ],
        ),
      ),
    );
  }

  Widget borderContainer() {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(12.0),
            child:
                Icon(Icons.location_on, size: 50.0, color: Colors.yellow[600]),
          ),
          RaisedButton(
            child: Text('Allow Location'),
            splashColor: Colors.yellow[700],
            color: Colors.yellow[600],
            textColor: Colors.black87,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0)),
            onPressed: () => {
                  Navigator.push(
                    context,
                    new MaterialPageRoute(
                      builder: (BuildContext context) => new HomePage(),
                    ),
                  )
                },
          ),
        ],
      ),
    );
  }

  Widget bottomSection(BuildContext context) {
    return Flexible(
      child: Container(
        padding: EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            FlatButton(
              child: Text('Back'),
              onPressed: () => {Navigator.of(context).pop()},
              padding: const EdgeInsets.all(10.0),
            ),
            buildNextButton(),
          ],
        ),
      ),
    );
  }

  Widget buildNextButton() {
    return _isButtonDisabled != false
        ? OutlineButton(
            child: Text('Next'),
            splashColor: Colors.lightBlueAccent,
            borderSide: BorderSide(width: 2, color: Colors.lightBlue),
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0)),
            onPressed: () => {
                  Navigator.push(
                    context,
                    new MaterialPageRoute(
                      builder: (BuildContext context) => new HomePage(),
                    ),
                  )
                },
            padding: const EdgeInsets.all(10.0),
          )
        : RaisedButton(
            child: Text('Next'),
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0)),
            onPressed: () => {
                  Fluttertoast.showToast(
                      msg: 'Please allow access to your location',
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIos: 1,
                      backgroundColor: Colors.yellow[600],
                      textColor: Colors.black87,
                      fontSize: 16.0)
                },
            padding: const EdgeInsets.all(10.0),
          );
  }
}
