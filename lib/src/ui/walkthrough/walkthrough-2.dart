import 'package:flutter/material.dart';
import 'package:animated_background/animated_background.dart';
import 'package:poppu/src/ui/walkthrough/walkthrough-3.dart';

class SecondWalkthroughPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SecondWalkthroughState();
  }
}

class _SecondWalkthroughState extends State<SecondWalkthroughPage>
    with TickerProviderStateMixin {
  String name = "";

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnimatedBackground(
        behaviour: BubblesBehaviour(),
        vsync: this,
        child: columnBody(),
      ),
    );
  }

  Widget columnBody() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        middleSection(),
        bottomSection(context),
      ],
    );
  }

  Widget middleSection() {
    return Expanded(
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'What\'s your name',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25.0),
            ),
            borderContainer(),
          ],
        ),
      ),
    );
  }

  Container borderContainer() {
    return Container(
      margin: EdgeInsets.all(20.0),
      width: 300.0,
      child: TextField(
        textAlign: TextAlign.center,
        keyboardType: TextInputType.text,
        onChanged: (value) {
          setState(() {
            name = value;
          });
        },
        textInputAction: TextInputAction.continueAction,
        textCapitalization: TextCapitalization.words,
        style: TextStyle(color: Colors.deepPurple, fontWeight: FontWeight.bold),
        maxLines: 1,
        decoration: InputDecoration(
          fillColor: Colors.white,
          filled: true,
          hintText: 'Enter Name',
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30.0),
            borderSide: BorderSide(color: Colors.black, width: 2.0),
          ),
        ),
      ),
    );
  }

  Widget bottomSection(BuildContext context) {
    return Flexible(
      child: Container(
        padding: EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            FlatButton(
              child: Text('Back'),
              onPressed: () => {Navigator.of(context).pop()},
              padding: const EdgeInsets.all(10.0),
            ),
            OutlineButton(
              child: Text('Next'),
              splashColor: Colors.lightBlueAccent,
              borderSide: BorderSide(width: 2, color: Colors.lightBlue),
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0)),
              onPressed: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (BuildContext context) =>
                                new ThirdWalkthroughPage(name)))
                  },
              padding: const EdgeInsets.all(10.0),
            ),
          ],
        ),
      ),
    );
  }
}
