import 'package:flutter/material.dart';

class FeedPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(4.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Icon(
                  Icons.account_circle,
                  size: 50.0,
                  color: Colors.grey,
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 15.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                                child: RichText(
                              text: TextSpan(children: [
                                TextSpan(
                                  text: 'Kai',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 18.0,
                                      color: Colors.black),
                                ),
                                TextSpan(
                                    text: " @kai",
                                    style: TextStyle(
                                        fontSize: 16.0, color: Colors.grey)),
                                TextSpan(
                                    text: " 3m",
                                    style: TextStyle(
                                        fontSize: 16.0, color: Colors.grey))
                              ]),
                              overflow: TextOverflow.ellipsis,
                            )),
                            flex: 5,
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(right: 4.0),
                              child: Icon(
                                Icons.expand_more,
                                color: Colors.grey,
                              ),
                            ),
                            flex: 1,
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 4.0),
                      child: Text(
                        "Hey there",
                        style: TextStyle(fontSize: 18.0),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        Divider(),
      ],
    );
  }
}
