import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class NotificationPage extends StatelessWidget {
  final FirebaseAuth _fireAuth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: FlatButton(
          child: Text('Logout'),
          onPressed: () => {signOutUser()},
        ),
      ),
    );
  }

  signOutUser() async {
    await _fireAuth.signOut();
  }
}
