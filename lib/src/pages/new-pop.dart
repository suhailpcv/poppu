import 'package:flutter/material.dart';

class NewPopPage extends StatelessWidget {
  final TextEditingController popTextController = TextEditingController();
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.yellow[700],
        title: Text(
          'Post a pop!',
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        actions: <Widget>[
          new Padding(
            padding: const EdgeInsets.all(10.0),
            child: new Row(
              children: <Widget>[
                new RaisedButton(
                  padding: const EdgeInsets.all(8.0),
                  textColor: Colors.white,
                  color: Colors.blue[600],
                  onPressed: () => {},
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(30.0),
                  ),
                  child: new Text("Pop"),
                ),
              ],
            ),
          ),
        ],
      ),
      body: _buildPopBody(),
      bottomNavigationBar: new BottomAppBar(
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.image),
              disabledColor: Colors.yellow[700],
              onPressed: () => {},
            ),
            // IconButton(
            //   icon: Icon(Icons.location_on),
            //   disabledColor: Colors.yellow[700],
            //   onPressed: () => {},
            // ),
          ],
        ),
      ),
    );
  }

  Widget _buildPopBody() {
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(15.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: Image.network(
                    "https://i.imgur.com/BoN9kdC.png",
                    fit: BoxFit.cover,
                    height: 50.0,
                    width: 50.0,
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Killer666',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Text(
                    'UiTM@SA001-Shah_Alam',
                    style: TextStyle(color: Colors.grey),
                  ),
                  Text(
                    '12.30pm',
                    style: TextStyle(color: Colors.grey),
                  ),
                ],
              ),
            ],
          ),
          TextField(
            maxLines: 10,
            maxLength: 200,
            textCapitalization: TextCapitalization.sentences,
            decoration: new InputDecoration(
                contentPadding: EdgeInsets.all(30.0),
                hintText: "What's on your mind?"),
            autofocus: true,
            controller: popTextController,
          ),
        ],
      ),
    );
  }
}
