import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:poppu/src/pages/new-pop.dart';
import 'package:poppu/src/pages/tabs/explore.dart';
import 'package:poppu/src/pages/tabs/feed.dart';
import 'package:poppu/src/pages/tabs/messages.dart';
import 'package:poppu/src/pages/tabs/notification.dart';

class HomePage extends StatefulWidget {
  final FirebaseAuth _fireAuth = FirebaseAuth.instance;

  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  FirebaseUser user;
  int _currentIndex = 0;
  final List<Widget> _children = [
    FeedPage(),
    ExplorePage(),
    MessagePage(),
    NotificationPage(),
  ];

  void onTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  getCurrentUser() async {
    user = await widget._fireAuth.currentUser();
  }

  @override
  void initState() {
    getCurrentUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage("https://i.imgur.com/BoN9kdC.png"),
                  ),
                ),
              ),
            ),
            ListTile(
              title: Text('Item 1'),
              onTap: () {
                // Update the state of the app
                // ...
              },
            ),
            ListTile(
              title: Text('Item 2'),
              onTap: () {
                // Update the state of the app
                // ...
              },
            ),
          ],
        ),
      ),
      appBar: AppBar(
        leading: Builder(
          builder: (BuildContext context) {
            return Padding(
              padding: EdgeInsets.all(15.0),
              child: GestureDetector(
                onTap: () => {Scaffold.of(context).openDrawer()},
                child: Container(
                  decoration: new BoxDecoration(
                    shape: BoxShape.circle,
                    image: new DecorationImage(
                        fit: BoxFit.cover,
                        // image: AssetImage('lib/assets/images/user-portrait.jpg')
                        image: new NetworkImage("https://i.imgur.com/BoN9kdC.png"),
                        ),
                  ),
                ),
              ),
            );
          },
        ),
        backgroundColor: Colors.yellow[700],
        title: Text(
          'POPPU',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 25,
            color: Colors.white,
          ),
        ),
        actions: <Widget>[
          new Padding(
            padding: const EdgeInsets.all(10.0),
            child: new Row(
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.near_me),
                  color: Colors.blue[600],
                  onPressed: () => {
                        Navigator.push(
                          context,
                          new MaterialPageRoute(
                            builder: (BuildContext context) => new NewPopPage(),
                          ),
                        ),
                      },
                ),
              ],
            ),
          ),
        ],
      ),
      body: _children[_currentIndex],
      bottomNavigationBar: new BottomAppBar(
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.bubble_chart),
              disabledColor: Colors.yellow[700],
              onPressed: _currentIndex == 0
                  ? null
                  : () => setState(() => _currentIndex = 0),
            ),
            IconButton(
              icon: Icon(Icons.explore),
              disabledColor: Colors.yellow[700],
              onPressed: _currentIndex == 1
                  ? null
                  : () => setState(() => _currentIndex = 1),
            ),
            IconButton(
              icon: Icon(Icons.chat_bubble),
              disabledColor: Colors.yellow[700],
              onPressed: _currentIndex == 2
                  ? null
                  : () => setState(() => _currentIndex = 2),
            ),
            IconButton(
              icon: Icon(Icons.notifications),
              disabledColor: Colors.yellow[700],
              onPressed: _currentIndex == 3
                  ? null
                  : () => setState(() => _currentIndex = 3),
            ),
          ],
        ),
      ),
    );
  }
}
